#Side Challenge 


*Xcode 11.6 is used to build this project.* 

###Pods Used: <br>
pod 'Alamofire', '~> 5.2' <br>
pod 'Kingfisher', '~> 5.0'<br>
pod 'RealmSwift'<br>

AlamoFire for netwroking layer abtraction. 
Kingfisher is for efficient image fetching 
RealmSwift for Local database storage. 

###Workflow: 
You can search user for any keyword. once list is loaded. It would have stored locally. It would be dispalyed even if there is no internet. 

Same goes for user details, If user details is visited once it would be also locally persisted. 

Project was compelted in 3 hours in 50 mins. 
Which include Netwrok layer, Local storage persistent manager, as well as few userful extension for the project. 